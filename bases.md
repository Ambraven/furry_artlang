# Táfärilùm

## Phonology

Different animals have different capabilities in regard to phonology.
In order to make it possible for as many species as possible to speak one language some liberties on the actual pronunciation are necessary.
This lead to having few different meaningful consonants and vowels.

Consonants: 
* labial : m /p~b~m/
* fricative : f /f~v~s~z~ʃ/
* alveolar : l /ɾ-l/
* trill : r /ʙ~r/
* plosive : t /t~d/
* clicks : · /ʘ~|~!~ǂ~ǁ/

Vowels:
* a : /a/
* i : /i/
* u : /u/

Tones:
* flat: (unmarked)
* high: ¨ -> ä ü ï
* low: ¯ -> ā ū ī (or a_ u_ i_ but for simplicity but it's not as pretty)
* ascendant : ^ -> â û î (or ' -> á í ú)
* descendant :  à ù ì (or i` for simplicity)

Note: for practical purposes, a tone can be marked after the vowel it affects.


The syllable structure is (C)V(C), because words can end with a consonant, however two consonants can't follow each other.
The language is rather vowel-heavy, making much more use of various vowels than consonants.


## Writing system

![how to create sounds](./alphabet_furry.jpeg)

## Grammar

This language is based on toki pona, because starting from a minimalistic framework seemed apropriate, both because fewer features means it is easier work with,
and because as the language is meant to be somewhat universal, simpler means easier to use for as many as possible.
The prononciation was made a bit more complicated to compensate the lack of consonantes and vowels, and keep a common ground, 
but there were few reasons to do something like that for the rest of the language.

### Sentence structure

The word order is traditionally subject, verb, complement (SVO). Contextual information can be given before the subject.
There are no capital letters at the beginning of a sentence, and sentences ends with a point when written with the latin writing system.

Any noun can be used as a verb.
Any noun after the pronoun ma or ta is a verb.
After any other subjet, the separator 'li' is used between the suject and the verb to tell them apart.
This is necessary because several nouns grouped together form a compound noun, as is explained below.
Compound nouns can be used as verb like regular nouns.

The complement is introduced by the separator 'tū'.

The contextual information at the start of the sentence ends with the separator 'la'. The context can be a simple word or a complete sentence.

### Compound nouns

We can create compound nouns by putting two words next to each other.
In such a case, the first noun changes some aspect of the second noun, like an adjective.
For example, if you put the words for "movement" and "house" next to each other in this order, you designate a thing with a roof that moves.
If you put the words in the reverse order, you designate a movement which is like a house in some way...

If you want to use more than 2 words, the rule is that a noun always modifies the noun just after it.
If you want a noun to modify a group of nouns following it, you can use the separator 'mi'.
For exemple, to say "a little yellow house", you can't just put the nouns for "little", "yellow" and "house" next to each other, because it would mean "a house with little yellow on it".
You'll have to say "little" "mi" "yellow house", which translate to "a yellow house which is little".

### Combining words

Sometimes, some association of nouns are so often used that the two nouns become effectively one word.
Combining words is done by putting the modifying word before the object word.

Example: 'ùidar' is mud (watered dirt)

Note that if a combination is common enough, the roots will often be modified to shorten the resulting word.

Example: taa (hard/solid) become tā in composed word.

Also, since two consonants can't be next to each other, a vowel can pass from after a consonant to before it.

Example: tu·afä is tu· (group) and faa.

The change of tone is kinda arbitrary. Newly composed words usually don't have such a change as it only appears with time.

### Negation

Negation is marked with the prefix 'fā-'.

To negate a compound word, 'fā-' is used on 'mi'.

"fāfirì tüi" means "a drink that is not cold".

"fāmi firì tüi" means "not a cold drink".

### Prepositions

Prepositions can either be used after a verb to add meaning to it or replace a verb, or at a start of the context group to indicate the kind of context.


* ir : prep	to, in order to, for
* a· : prep by, because of
* uti : prep to, towards, until
* tam : prop from, since
* mï : prep at, here, now

ma uti liä tarifä : I go to that hill.

ma tüi ir fāla· tuï : I drink in order to not [be thristy]? (wanting a drink).

ma tüi a· la· tuï : I drink because [I am thirsty]? (wanting a drink).

ma mï : I'm here

ma mï lïa tarifä : I [am] at this hill.

ma tam tarifä : I [come] from a hill.


* a· <cause> la <effect> : because <cause>, <effect>
* ir <goal> la <action toward goal> : for <goal>, <action toward goal>
* fu <contradictory stuff> la <other stuff> : Even so <contradictory stuff>, <other stuff>
* i <complementary stuff> la <principal stuff> : In addition to <complementary stuff>, <principal stuff>* 

ir ta ali la ta irü ali : (For you) [In order to] to eat, you gather food.

uti ta ali la ta la· ali : Until you eat, you [are hungry]? (want food).

### Conjunction

These words can semantically link two types of equivalent word structures:

* i : and
* u : or
* fu : but
* a· : because
* ir : until

ma la· ali i ta la· tüi : I want food and you want a drink.

ma la· tüi ta tüi e ifiri tüi : I want a drink because you drink every drink.

'i' can be ignored if a group of words is already using a separator:

ra li tüi li ali : They drink and they eat.

ma iü e ali e tüi : I see food and drink.
 

### Question

To form a question, just use '·a' as a preposition which indicates what the question is about, or in place of the subject of the question.

ta ·a tüi? : Are you drinking?

ta tüi e ·a? : What are you drinking?

·a li tüi? : Who/what is drinking?

·a fima la ta tüi? When do you drink?

ra ·a li tüi ·a li ali? : Do they drink and do they eat?

ra ·a li tüi u li ali? : Do they drink or eat?

### Time and space

Time and space are treated the same syntactically. To indicate if an action is in the past, begin the sentence with "tam la" and for an action in the future use "uti la".
If you need to specify that you're talking about time, you can use "fima".

tam fima la/tam la... : in the past,...

uti fima la/uti la... : in the future...

If you want to specify that you're talking about space, you can use "ūü".

tam ūü la... : moving from a place,...

uti ūü la... : moving toward a place,...

## Numbers
Animals don't all have the same number of digits so they chose a common ground.
This doesn't necessarilly help equine or even-toed ungulates counting on their fingers, but, well, you still have to have a practical base, so... 
At least lots of species have more than 3 fingers on one member.
All the practical stuff about seximal is just a bonus. (see https://www.seximal.net/ )

 * 0 : fāri
 * 1 : rì
 * 2 : ·ù
 * 3 : ta
 * 4 : tú
 * 5 : lí
 * 10 : mä
 * 11 : mä rì
 * 12 : mä ·ù
 * 13 : mä ta
 * 14 : mä tú
 * 15 : mä lí
 * 20 : ·umä
 * 30 : tamä
 * 40 : túmä
 * 50 : límä
 * 100 : mämimä


## vocabular
* a : (surprise exclamation)
* ä : (pleased exclamation)
* ā : (angry exclamation)
* â : (thing got better exclamation)
* à : (thing got worst exclamation)

_

* ma : self, I, me
* ta : you
* ra : she, he, it, they

_
 
* tū : (introduce a direct object)
* la : (between adverb or phrase of context and sentence)
* li : (between any subject, except ma and ta, and its verb; also used to introduce a new verb for the same subject)
* ü : sep	O (vocative or imperative)
* mi : pi ぴ (jp), 的 (zh) de : sep	of, belonging to

_

 * fā- : non-
  
_

* ūä : danger
* ri : thing
* la· : to want, need, wish, have to, must, will, should
* ali : food
* tüi : drink
* fif : sex, sexuality
* raā· : conflict, disharmony, competition, fight, war, battle, attack, blow, argument, physical or verbal violence
* lùm : peace, quite, stable
* uùm : death, to die
* mía : life, to live
* aâm : good
* maà : bad
* firì : cold
* firä· : cool, lukewarm 
* laâ : warm, warmth
* ä· : hot, heat
* ä·ir : fire
* ū : down
* ï : high, up, above, top, over, on
* mai : hand, prehensible apendage
* mui : arm, appendage with a hand
* tui : leg, appendage with a foot
* tai : foot, walking appendage
* iü : eye, to see
* mii : body
* miï : chest, torso
* mua : mouth
* faa : face
* aùl : hear, to ear
* àûl : sound
* fia : noze, to smell
* fïà : a smell
* tul : tongue, to taste
* tùl : taste
* lïu : to touch
* ràu : hair
* maïri : tool
* färi : fun, playing, game, recreation, art, entertainment
* taa : hard(solid)
* ·ui : solid
* fàul : wind
* fà : air
* tar : earth, dirt
* tarutā : stone
* ùi : water
* ùitā : semi-solid, paste
* tu· : group
* fuūl : to sleep
* mi· : big, tall
* mul : small
* tiä : young, new
* a·ū : few
* ā·ù : less
* uta : many, very, much, several, a lot, abundant, numerous
* ûtä : more
* fïìū : long
* āā : flat
* āû : moon, planete
* lâä : sun, light
* mï : be (located) in/at/on
* la· : side, hip, next to
* üū : hole
* ūü : (outdoor) area
* fi : female
* fu : male
* tūa : back, behind
* fâlù : love
* ·a· : blunder, accident, mistake, destruction, damage, breaking
* fìtā : long, mostly hard object, e.g. rod, stick, branch
* tu·afä : leader
* ū· : end, tip
* fâä : silly, crazy, foolish, drunk, strange, stupid, weird
* tarifä : bump, hill, mountain
* lïa : this
* liä : that
* ·a : what, which, wh- (question word)
* fïù : plant
* ·āí : animal
* ·u : only, sole
* muául : talking, a talk, a story
* iu· : away, absent, missing
* rū : event
* ·ï : beginning
* tū· : to rest
* räu : uncooked
* a·ï : spicy
* ta·ī : cooked
* uti : to go
* üri : give, put, send, place, release, emit, cause
* imū : to contain
* tam : to come
* táäf : to hapen
* mim : to start
* tafū : to arrive
* ·īam : to be able to
* äìūf : to use
* fima : time, period of time, moment, duration, situation
* túa : different
* maïmū : to have
* tūm : inside
* ·īf : cut
* mü· : container, box, bowl, cup, glass
* täli : grain, cereal

_

* ùlarù : reptile
* tùàríl : amphibian
* fal : fish
* rùüa : sea creature
* ùi·ù : insect
* fí·i·am : spider
* ìürū : bird, winged animal
* má : person

_

* auù : herb
* ílúti : tree
* árià : flower
* īlùrù : fruit
* líū·äf : mushroom

_

* fāri : nothing
* ifiri : everything

_

* rīā : black, dark
* arì : white, light (colour)
* müìm : colour
* li·ï : yellow
* rìtu : blue
* ūmū : red
* itä : green

_

* mal : candy, sweet food
* irü : to get
* uùmürima : killer (one who give death)
* mi·am : adult, important
*  : difficult
* taräl : metal
*  : community
*  : pack
*  : land, country
*  : wait
*  : dirty
*  : market
*  : to become
*  : chance
*  : wood
*  : to be allowed to
*  : also/too
*  : even
*  : indeed
*  : soul
*  : paint
*  : listen
*  : head
*  : mind
*  : learning
*  : the main
*  : clothe
*  : fabric
*  : to reduce
*  : to shrink
*  : string
*  : rope
*  : chair
*  : paper
*  : carnet
*  : book
*  : to look at
*  : to read
*  : visually
*  : window
*  : door
*  : parent, mother, father
*  : money, material wealth, currency, dollar, capital
*  : rear end
*  : extra, addtional
*  : number
*  : way, manner, custom, road, path, doctrine, system, method
*  : word
*  : name
*  : open, turn on
*  : screw up, fuck up, botch, ruin, break, hurt, injure, damage, spoil, ruin
*  : activity, work, deed, project
*  : brother
*  : feelings, emotion, heart
*  : glass
*  : simple
*  : same, similar, equal, of equal status or position
* mït : outside, surface, skin, shell, bark, shape, peel
*  : superior, elevated, religious, formal
*  : physical state
*  : circle, wheel
*  : sphere, ball
*  : cycle
*  : new, fresh, another, more
*  : front
*  : wall
*  : picture, image
*  : knowledge, wisdom, intelligence, understanding
*  : land mammal
*  : horizontal surface, e.g furniture, table, chair, pillow, floor
*  : liquid
*  : sauce
*  : language, speech, communication
*  : indoor constructed space, e.g. house, home, room, building
*  : energy, strength, power
* faūl : space, void
* utiri : to create, to become (uti (to go/tawa) + ri (thing))
* äìfumü· : remplir (äìūf (to use) + mü· (container))
* ·ít : to move, movement
* fá· : to turn, to rotate

_

* riämat : hello/bye (litteraly "good things to you")
* táfäri : Entity ( from táäf : happen, and ri : thing. litterally "Thing that make happen")
* fāūü : Ether ( litteraly "non-area"/"non-place" )
* rama : themselve (litteraly)
* faluláä : space lights/sun, celestial body. (lâä : sun, light + faūl (space))
* ūüri : scatter (ūü (outdoor area) + üri (put))
* táfärilùm : The name of the language ( litérally peace of the Entities )
* läfima : a sun period, a day
* miū· : botom, ass
* miū·tū· : resting on own ass, sitting
* a·firìürū : bird from the cold -> Razorbill
* fāmuíürū : bird with no arm -> Penguin
